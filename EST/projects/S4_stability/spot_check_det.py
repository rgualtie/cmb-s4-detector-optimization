
import argparse 
from helper_funcs import *

parser = argparse.ArgumentParser()
parser.add_argument("--spec", type=int, default=30, 
        help="<30, 40, 85, 95, 145, 155, 220, or 270>")
parser.add_argument("--mode", default="sci", 
        help="<sci or lab>")
parser.add_argument("--sci_Ic", type=float, default=.1,
        help="<sci critical current [A]>")
parser.add_argument("--lab_Ic", type=float, default=.1,
        help="<lab critical current [A]>")
parser.add_argument("--sci_Rn", type=float, default=2e-3,
        help="<sci normal resistance [Ohms]>")
parser.add_argument("--lab_Rn", type=float, default=4e-3,
        help="<lab normal resistance [Ohms]>")
parser.add_argument("--L", type=float, default=3e-7,
        help="<inductance [H]>")
parser.add_argument("--Cx", type=float, default=1e-12,
        help="<heat capacity at 160mK [J/K]>")
parser.add_argument("--R_frac", type=float, default=.5,
        help="<target R/Rn for chosen TES>")
args, unknown = parser.parse_known_args()
if args.mode == "sci": TES_index = 0
else: TES_index = 1
det, nm = load_det(args.spec, args.mode)
det.TESs[0].Ic = args.sci_Ic 
det.TESs[1].Ic = args.lab_Ic
det.TESs[0].Rn = args.sci_Rn
det.TESs[1].Rn = args.lab_Rn
det.L = args.L
det.Cx = args.Cx
det = det.copy_detector()
det.find_bias(R_frac=args.R_frac, TES_index=TES_index)
nm.det = det
det.print_state()        

tau_p, tau_m, _, _ = det.get_taus()
print "stable:", det.stable()
print "overdamped:", det.overdamped()
print "betaI:", det.get_beta()
print "alphaI:", det.get_alpha()
print "tau+:", tau_p 
print "tau-:", tau_m
print
plt.figure(figsize=(12, 20))
savename = raw_input("Savename for plot (hit enter to not save): ")
title = raw_input("Title for plot: ")
nm.total_multiplexed_noise(plot_NEI=True, plot_NEP=True, title=title)
plt.subplot(211)
plt.ylim((1e-2, 4e3))
plt.subplot(212)
plt.ylim((1e-2, 1e4))
print
savedir = "/home/osherso2/documents/EST/projects/S4_stability/"
savedir = os.path.join(savedir, "results/spot_checker")
savename = os.path.join(savedir, savename)
if len(savename) > 1: 
    print "saving to ", savename
    plt.savefig(savename, figsize=(12, 20), pad_inches=0, bbox_inches="tight")
plt.show()
