import os, sys
import numpy
import matplotlib.pyplot as plt
import matplotlib.collections as collections

from DET_model import *
from NOISE_model import *

TESs = []

det_params = {}
# Device parameters
det_params['TESs'] = TESs     
det_params['Tx'] = 0 
det_params['Cx'] = 0
det_params['Gx'] = 0
det_params['betaC'] = 0
det_params['betaG'] = 0
det_params['num_rows'] = 0
det_params['L'] = 0
det_params['Rsh'] = 0
det_params['Rb'] = 0
det_params['Rl'] = 0
det_params['Rs'] = 0
det_params['Popt'] = 0
det_params['center_freq'] = 0
det_params['bandwidth'] = 0
# Initialize with the following state
det_params['Vb'] = 0 
det_params['Tb'] = 0 
det_params['R'] = 0 
det_params['I'] = 0 
det_params['T'] = 0 
det = detector(**det_params)

noise_params = {}
noise_params["det"] = None
noise_params["readout_plateau"] = 2.5e-12
noise_params["pink_knee"] = 2.
noise_params["roll_off"] = 2e6
noise_params["m"] = 2.
noise_params["dwell_by_tau"] = 6.

specs = [30, 40, 85, 95, 145, 155, 220, 270]
transitions = ["sci", "lab"]

def load_det(spec, transition="sci", lib_dir="./det_lib"):

    assert(spec in specs)
    assert(transition in transitions)

    name = "det%03d_" % spec + transition

    loadname = os.path.join(lib_dir, name+".pkl")
    ndet = det.load_pkl(filename=loadname)
    noise_params["det"] = ndet.copy_detector()
    det_noise = noise(**noise_params)
    return ndet, det_noise 

def dataframe_dir(spec, mode, Ic, mkdir=False):
                                                                                
    Ic_str = ("Ic_%01.01eA" % Ic).replace(".", "p")                             
    dirname = "/home/osherso2/documents/EST/"                                  
    dirname1 = os.path.join(dirname, "projects/S4_stability/results")         
    if mkdir and not os.path.exists(dirname1): os.mkdir(dirname1)
    dirname2 = os.path.join(dirname1, Ic_str)                                   
    if mkdir and not os.path.exists(dirname2): os.mkdir(dirname2)
    dirname3 = os.path.join(dirname2, "%s_config" % mode)                       
    if mkdir and not os.path.exists(dirname3): os.mkdir(dirname3)
    dirname4 = os.path.join(dirname3, "det%03dGHz" % spec)                      
    if mkdir and not os.path.exists(dirname4): os.mkdir(dirname4)
    return dirname1, dirname2, dirname3, dirname4   

def det_iterate(func):

    res = []
    for spec in specs:
        for mode in transitions:
            print spec, mode
            det, noise = load_det(spec, transition=mode)
            res.append(func(det, noise))
            print
    return res

def twiny(fig, x, y, label="", color=(1,0,0)):

    a = fig.axes[-1]
    b = a.twinx()

    b.plot(x, y, color=color)
    b.set_ylabel(label, color=color)
    b.tick_params('y', colors=color)

def twinx(fig, x, y, label="", color=(1,0,0)):

    a = fig.axes[-1]
    b = a.twiny()
    b.plot(x, y, color=color)
    b.set_xlabel(label, color=color)
    b.tick_params('x', colors=color)

def plot_2D_R(det, title="", savename=None, cmap=None):

    labeler_I = lambda X: ["%.01e" % x for x in X]                            
    labeler_T = lambda X: ["%d" % int(x) for x in X]                            
                                                                                
    Imax = 0                                                                
    Tmax = 0                                                                
    for tes in det.TESs:                                                   
        Imax = max(Imax, tes.Ic)                                            
        Tmax = max(Tmax, tes.Tc)                                            
    Tmin = det.Tb                                                          
    Imin = 0                                                                
    I_arr = numpy.linspace(Imin, Imax, 1000)
    T_arr = numpy.linspace(Tmin, Tmax, 1000)                                
    R_arr = numpy.zeros((len(I_arr), len(T_arr)))                           
    R_arr[:] = nan                                                          
    for i, I in enumerate(I_arr):                                       
        for j, T in enumerate(T_arr):                                          
            R_arr[i, j] = det.get_R(I=I, T=T)                              
    R_arr = R_arr*1e3                                                               
    Rmin = numpy.nanmin(R_arr)                                              
    Rmax = numpy.nanmax(R_arr)                                              
                                                                            
    if cmap is None: cmap = plt.cm.nipy_spectral
    plt.figure(figsize=(12, 7))                                             
    plt.matshow(R_arr, vmin=Rmin, vmax=Rmax, cmap=cmap)                       
    ax = plt.gca()                                                          
    cb = plt.colorbar(shrink=.75)                                           
    cb.ax.set_title(r"[$m\Omega$]")                                         
                                                                            
    ax.set_yticks(numpy.arange(len(I_arr))[::80])                                     
    ax.set_xticks(numpy.arange(len(T_arr))[::120])                                
    ax.set_yticklabels(labeler_I(I_arr*1e3)[::80])                                      
    ax.set_xticklabels(labeler_T(T_arr*1e3)[::120])                            
    
    plt.title(title, y=1.08)
    plt.ylabel(r"I [$mA$]")                                              
    plt.xlabel(r"T [$mK$]")                                                 
    plt.tight_layout()    
    if savename is None: plt.show()
    else: 
        plt.show()
        plt.savefig(savename, bbox_inches="tight", figsize=(15, 15))
    plt.cla()
    plt.clf()
    plt.close()        

def grid_plot_RL(arr, R_arr, L_arr, units="", title="", c_ax=None, mode="sci", 
              lims=(numpy.nan,numpy.nan), savename=None, cmap=None):

    print "plotting ", title
    labeler = lambda X: ["%.01f" % x for x in X]

    nlims = lims[:]
    if numpy.isnan(nlims[0]): nlims = (0.99*numpy.nanmin(arr), nlims[1])
    if numpy.isnan(nlims[1]): nlims = (nlims[0], 1.01*numpy.nanmax(arr))

    plt.figure(figsize=(12, 7))
    if cmap is None: cmap = plt.cm.nipy_spectral
    plt.matshow(arr, vmin=nlims[0], vmax=nlims[1], cmap=cmap)
    ax = plt.gca()
    if c_ax is not None: cb = plt.colorbar(shrink=.75, ticks=c_ax[0])
    else: cb = plt.colorbar(shrink=.75)
    cb.ax.set_title(units)
    if c_ax is not None: cb.set_ticklabels(c_ax[1])

    ax.set_yticks(numpy.arange(len(R_arr)))
    ax.set_xticks(numpy.arange(len(L_arr))[::3])
    ax.set_yticklabels(labeler(R_arr*1e3))
    ax.set_xticklabels(labeler(L_arr*1e6)[::3])

    ax.set_yticks(numpy.arange(-.5, len(R_arr)), minor=True)
    ax.set_xticks(numpy.arange(-.5, len(L_arr)), minor=True)
    ax.grid(which='minor')

    plt.title(title, y=1.08)
    plt.xlabel(r"L [$\mu H$]")
    if mode=="sci": plt.ylabel(r"sci $R_n$ [$m\Omega$]")
    else: plt.ylabel(r"lab $R_n$ [$m\Omega$]")
    plt.tight_layout()
    if savename is None: plt.show()
    else: 
        plt.show()
        plt.savefig(savename, bbox_inches="tight")

def grid_plot_Rf(arr, R_arr, f_arr, units="", title="", c_ax=None, mode="sci", 
              lims=(numpy.nan,numpy.nan), savename=None, cmap=None):

    print "plotting ", title
    labeler = lambda X: ["%.01f" % x for x in X]

    nlims = lims[:]
    if numpy.isnan(nlims[0]): nlims = (0.99*numpy.nanmin(arr), nlims[1])
    if numpy.isnan(nlims[1]): nlims = (nlims[0], 1.01*numpy.nanmax(arr))

    plt.figure(figsize=(12, 7))
    if cmap is None: cmap = plt.cm.nipy_spectral
    plt.matshow(arr, vmin=nlims[0], vmax=nlims[1], cmap=cmap)
    ax = plt.gca()
    if c_ax is not None: cb = plt.colorbar(shrink=.75, ticks=c_ax[0])
    else: cb = plt.colorbar(shrink=.75)
    cb.ax.set_title(units)
    if c_ax is not None: cb.set_ticklabels(c_ax[1])

    ax.set_yticks(numpy.arange(len(R_arr)))
    ax.set_xticks(numpy.arange(len(f_arr))[::2])
    ax.set_yticklabels(labeler(R_arr*1e3))
    ax.set_xticklabels(labeler(f_arr)[::2])

    ax.set_yticks(numpy.arange(-.5, len(R_arr)), minor=True)
    ax.set_xticks(numpy.arange(-.5, len(f_arr)), minor=True)
    ax.grid(which='minor')

    plt.title(title, y=1.08)
    if mode == "sci":
        plt.xlabel(r"(sci R)/(sci $R_n$)")
        plt.ylabel(r"sci $R_n$ [$m\Omega$]")
    if mode == "lab":
        plt.xlabel(r"(lab R)/(lab $R_n$)")
        plt.ylabel(r"lab $R_n$ [$m\Omega$]")
    plt.tight_layout()
    if savename is None: plt.show()
    else: 
        plt.show()
        plt.savefig(savename, bbox_inches="tight")

"""
def grid_plot_RL2(arr1, arr2, R_arr, L_arr,
        label1="", label2="",
        units="", title="", c_ax=None, 
        lims=(numpy.nan,numpy.nan), 
        savename=None, cmap=None):

    def triatpos(pos=(0,0), rot=0):
        r = numpy.array([[-1,-1],[1,-1],[1,1],[-1,-1]])*.5
        rm = [[numpy.cos(numpy.deg2rad(rot)), 
              -numpy.sin(numpy.deg2rad(rot))],
              [numpy.sin(numpy.deg2rad(rot)),
               numpy.cos(numpy.deg2rad(rot))]]
        r = numpy.dot(rm, r.T).T
        r[:,0] += pos[0]
        r[:,1] += pos[1]
        return r

    nlims1 = lims[:]
    nlims2 = lims[:]
    if numpy.isnan(lims[0]): 
        nlims1 = (numpy.nanmin(arr1), nlims1[1])
        nlims2 = (numpy.nanmin(arr2), nlims2[1])
    if numpy.isnan(lims[1]): 
        nlims1 = (nlims1[0], numpy.nanmax(arr1))
        nlims2 = (nlims2[0], numpy.nanmax(arr2))
    nlims = (min(nlims1[0], nlims2[0]), max(nlims1[1], nlims2[1]))


    def triamatrix(a, ax, rot=0):
        segs = []
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                if numpy.isnan(a[i][j]): continue 
                segs.append(triatpos((j,i), rot=rot))
        col = collections.PolyCollection(segs, cmap=cmap)
        col.set_clim(vmin=nlims[0], vmax=nlims[1])
        col.set_array(a.flatten())
        ax.add_collection(col)
        return col

    print "plotting ", title
    labeler = lambda X: ["%.01f" % x for x in X]

    fig, ax = plt.subplots(figsize=(12, 7))
    ax.annotate("", xy=(0,-.095), xytext=(0,-.135), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.1), xytext=(.025,-.1), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.133), xytext=(.024,-.096), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate(label1, xy=(.025,-.095),
            xycoords="axes fraction",
            ha='left', va='top')

    ax.annotate("", xy=(.021,-.145), xytext=(.021,-.185), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.179), xytext=(.024,-.179), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.183), xytext=(.024,-.146), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate(label2, xy=(.025,-.145),
            xycoords="axes fraction",
            ha='left', va='top')

    if cmap is None: cmap = plt.cm.nipy_spectral

    im1 = ax.imshow(arr1, vmin=nlims[0], vmax=nlims[1], cmap=cmap)
    im2 = triamatrix(arr2, ax, rot=90)

    if c_ax is not None: cb = fig.colorbar(im1, shrink=.5, ticks=c_ax[0])
    else: cb = fig.colorbar(im1, shrink=.5)
    cb.ax.set_title(units)
    if c_ax is not None: cb.set_ticklabels(c_ax[1])

    ax.set_yticks(numpy.arange(len(R_arr)))
    ax.set_xticks(numpy.arange(len(L_arr))[::3])
    ax.set_yticklabels(labeler(R_arr*1e3))
    ax.set_xticklabels(labeler(L_arr*1e6)[::3])

    ax.set_yticks(numpy.arange(-.5, len(R_arr)), minor=True)
    ax.set_xticks(numpy.arange(-.5, len(L_arr)), minor=True)
    ax.grid(which='minor')

    plt.title(title, y=1.08)
    plt.xlabel(r"L [$\mu H$]")
    plt.ylabel(r"Rn [$m\Omega$]")
    plt.tight_layout()
    if savename is None: plt.show()
    else: 
        plt.show()
        plt.savefig(savename, bbox_inches="tight")

def grid_plot_Rf2(arr1, arr2, R_arr, f_arr,
        label1="", label2="",
        units="", title="", c_ax=None, 
        lims=(numpy.nan,numpy.nan), 
        savename=None, cmap=None):

    def triatpos(pos=(0,0), rot=0):
        r = numpy.array([[-1,-1],[1,-1],[1,1],[-1,-1]])*.5
        rm = [[numpy.cos(numpy.deg2rad(rot)), 
              -numpy.sin(numpy.deg2rad(rot))],
              [numpy.sin(numpy.deg2rad(rot)),
               numpy.cos(numpy.deg2rad(rot))]]
        r = numpy.dot(rm, r.T).T
        r[:,0] += pos[0]
        r[:,1] += pos[1]
        return r

    def triamatrix(a, ax, rot=0):
        segs = []
        for i in range(a.shape[0]):
            for j in range(a.shape[1]):
                if numpy.isnan(a[i][j]): continue 
                segs.append(triatpos((j,i), rot=rot))
        col = collections.PolyCollection(segs, cmap=cmap)
        col.set_clim(vmin=nlims[0], vmax=nlims[1])
        col.set_array(a.flatten())
        ax.add_collection(col)
        return col

    print "plotting ", title
    labeler = lambda X: ["%.01f" % x for x in X]

    nlims1 = lims[:]
    nlims2 = lims[:]
    if numpy.isnan(lims[0]): 
        nlims1 = (numpy.nanmin(arr1), nlims1[1])
        nlims2 = (numpy.nanmin(arr2), nlims2[1])
    if numpy.isnan(lims[1]): 
        nlims1 = (nlims1[0], numpy.nanmax(arr1))
        nlims2 = (nlims2[0], numpy.nanmax(arr2))
    nlims = (min(nlims1[0], nlims2[0]), max(nlims1[1], nlims2[1]))

    fig, ax = plt.subplots(figsize=(12, 7))
    ax.annotate("", xy=(0,-.095), xytext=(0,-.135), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.1), xytext=(.025,-.1), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.133), xytext=(.024,-.096), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate(label1, xy=(.025,-.095),
            xycoords="axes fraction",
            ha='left', va='top')

    ax.annotate("", xy=(.021,-.145), xytext=(.021,-.185), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.179), xytext=(.024,-.179), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate("", xy=(-.003,-.183), xytext=(.024,-.146), 
            xycoords="axes fraction",
            textcoords="axes fraction",
            arrowprops=dict(arrowstyle="-"))
    ax.annotate(label2, xy=(.025,-.145),
            xycoords="axes fraction",
            ha='left', va='top')

    if cmap is None: cmap = plt.cm.nipy_spectral

    im1 = ax.imshow(arr1, vmin=nlims[0], vmax=nlims[1], cmap=cmap)
    im2 = triamatrix(arr2, ax, rot=90)

    if c_ax is not None: cb = fig.colorbar(im1, shrink=.5, ticks=c_ax[0])
    else: cb = fig.colorbar(im1, shrink=.5)
    cb.ax.set_title(units)
    if c_ax is not None: cb.set_ticklabels(c_ax[1])

    ax.set_yticks(numpy.arange(len(R_arr)))
    ax.set_xticks(numpy.arange(len(f_arr))[::2])
    ax.set_yticklabels(labeler(R_arr*1e3))
    ax.set_xticklabels(labeler(f_arr)[::2])

    ax.set_yticks(numpy.arange(-.5, len(R_arr)), minor=True)
    ax.set_xticks(numpy.arange(-.5, len(f_arr)), minor=True)
    ax.grid(which='minor')

    plt.title(title, y=1.08)
    plt.xlabel(r"R/Rn")
    plt.ylabel(r"Rn [$m\Omega$]")
    plt.tight_layout()
    if savename is None: plt.show()
    else: 
        plt.show()
        plt.savefig(savename, bbox_inches="tight")
"""
