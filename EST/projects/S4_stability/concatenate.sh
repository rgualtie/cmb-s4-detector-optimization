#!/bin/bash

cd /home/osherso2/documents/EST/projects/S4_stability

EST=/home/osherso2/documents/EST/
PYTHONPATH=$PYTHONPATH:$EST/physics_models
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability/det_lib
export PYTHONPATH


e="/home/osherso2/documents/EST/projects/S4_stability/logs/concat_err"
o="/home/osherso2/documents/EST/projects/S4_stability/logs/concat_out"
q="secondary"
w="03:00:00"
n="concat" 
sbatch $args -t $w -p $q -J $n -e $e -o $o ./concat_dataframes.py
