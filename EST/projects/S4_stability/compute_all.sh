#!/bin/bash

cd /home/osherso2/documents/EST/projects/S4_stability

EST=/home/osherso2/documents/EST/
PYTHONPATH=$PYTHONPATH:$EST/physics_models
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability/det_lib
export PYTHONPATH


a="append"
e="/home/osherso2/documents/EST/projects/S4_stability/logs/compute_err"
o="/home/osherso2/documents/EST/projects/S4_stability/logs/compute_out"
rm $e 
rm $o
args=" -e "$e" -o "$o" --open-mode="$a
Ic=.01
for s in 30 40 85 95 145 155 220 270
do
    q="secondary"; w="04:00:00"
    n="com"$s"sci" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 0 $Ic;
    n="com"$s"lab" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 1 $Ic;

    q="astro-physics"; w="06:00:00"
    n="com"$s"sci" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 0 $Ic;
    n="com"$s"lab" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 1 $Ic;
done
Ic=.05
for s in 30 40 85 95 145 155 220 270
do
    q="secondary"; w="02:00:00"
    n="com"$s"sci" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 0 $Ic;
    n="com"$s"lab" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 1 $Ic;

    q="astro-physics"; w="03:00:00"
    n="com"$s"sci" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 0 $Ic;
    n="com"$s"lab" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 1 $Ic;
done
Ic=.1
for s in 30 40 85 95 145 155 220 270
do
    q="secondary"; w="01:00:00"
    n="com"$s"sci" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 0 $Ic;
    n="com"$s"lab" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 1 $Ic;

    q="astro-physics"; w="01:30:00"
    n="com"$s"sci" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 0 $Ic;
    n="com"$s"lab" 
    sbatch $args -t $w -p $q -J $n ./compute_dataframe.py $s 1 $Ic;
done

