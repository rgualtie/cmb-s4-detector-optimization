#!/bin/python 

import os
import numpy
import pandas
from helper_funcs import dataframe_dir, nan
from dataset_tools import dataset

keyword_arr_Rf = ["Vb", "R", "I", "T", "betaI", "alphaI"]
keyword_arr_RL = ["tau+", "tau-", "tot/det", "tot/pht"]
   
for Ic in [.1, .05, .01]:
    print "Ic: %.2e A" % Ic
    for mode in ["sci", "lab"]:
        print "  mode: ", mode
        dataframes = []

        lims = {}
        lims["Vb"] = [(nan, nan)] 
        lims["R"] = [(nan, nan)] 
        lims["I"] = [(nan, nan)] 
        lims["T"] = [(nan, nan)] 
        lims["betaI"] = [(nan, nan)] 
        lims["alphaI"] = [(nan, nan)] 
        lims["mid_tau+"] = [(nan, nan)] 
        lims["mid_tau-"] = [(nan, nan)] 
        lims["avg_tau+"] = [(nan, nan)] 
        lims["avg_tau-"] = [(nan, nan)] 
        lims["mid_tot/det"] = [(nan, nan)] 
        lims["mid_tot/pht"] = [(nan, nan)] 
        lims["avg_tot/det"] = [(nan, nan)] 
        lims["avg_tot/pht"] = [(nan, nan)] 

        for spec in [30, 40, 85, 95, 145, 155, 220, 270]:
            print "    spec", spec

            _, _, target, dirname = dataframe_dir(spec, mode, Ic)
            dfname = os.path.join(dirname, "dataframe")
            if not os.path.exists(dfname):
                print Ic, mode, spec, "MISSING!"
                print dfname, "\n"
                exit()
            df = pandas.read_pickle(dfname)
            df["tot/det"] = df["NEP_tot"]/df["NEP_det"]
            df["tot/pht"] = df["NEP_tot"]/df["NEP_pht"]
            dataframes.append(df)

            # This is intensive and not necessary across all specs
            if spec in [40, 85, 95, 145, 220]: continue 

            d = dataset(df, mode=mode)

            lims_mid_RL = d.get_RL_lims(keyword_arr=keyword_arr_RL, r_frac=.5)
            lims_avg_RL = d.get_RL_lims(keyword_arr=keyword_arr_RL, r_frac=None)
            lims_Rf = d.get_Rf_lims(keyword_arr=keyword_arr_Rf)

            lims["Vb"].append((lims_Rf["Vb"]))
            lims["R"].append((lims_Rf["R"]))
            lims["I"].append((lims_Rf["I"]))
            lims["T"].append((lims_Rf["T"]))
            lims["betaI"].append((lims_Rf["betaI"]))
            lims["alphaI"].append((lims_Rf["alphaI"]))
            lims["mid_tau+"].append((lims_mid_RL["tau+"]))
            lims["mid_tau-"].append((lims_mid_RL["tau-"]))
            lims["mid_tot/det"].append((lims_mid_RL["tot/det"]))
            lims["mid_tot/pht"].append((lims_mid_RL["tot/pht"]))
            lims["avg_tau+"].append((lims_avg_RL["tau+"]))
            lims["avg_tau-"].append((lims_avg_RL["tau-"]))
            lims["avg_tot/det"].append((lims_avg_RL["tot/det"]))
            lims["avg_tot/pht"].append((lims_avg_RL["tot/pht"]))

        dataframe = pandas.concat(dataframes)
        dataframe["Vb_lims_0"] = numpy.nanmin(lims["Vb"])
        dataframe["Vb_lims_1"] = numpy.nanmax(lims["Vb"])
        dataframe["R_lims_0"] = numpy.nanmin(lims["R"]) 
        dataframe["R_lims_1"] = numpy.nanmax(lims["R"]) 
        dataframe["I_lims_0"] = numpy.nanmin(lims["I"]) 
        dataframe["I_lims_1"] = numpy.nanmax(lims["I"]) 
        dataframe["T_lims_0"] = numpy.nanmin(lims["T"]) 
        dataframe["T_lims_1"] = numpy.nanmax(lims["T"]) 
        dataframe["betaI_lims_0"] = numpy.nanmin(lims["betaI"]) 
        dataframe["betaI_lims_1"] = numpy.nanmax(lims["betaI"]) 
        dataframe["alphaI_lims_0"] = numpy.nanmin(lims["alphaI"]) 
        dataframe["alphaI_lims_1"] = numpy.nanmax(lims["alphaI"]) 
        dataframe["mid_tau+_lims_0"] = numpy.nanmin(lims["mid_tau+"]) 
        dataframe["mid_tau+_lims_1"] = numpy.nanmax(lims["mid_tau+"]) 
        dataframe["mid_tau-_lims_0"] = numpy.nanmin(lims["mid_tau-"]) 
        dataframe["mid_tau-_lims_1"] = numpy.nanmax(lims["mid_tau-"]) 
        dataframe["mid_tot/det_lims_0"] = numpy.nanmin(lims["mid_tot/det"]) 
        dataframe["mid_tot/det_lims_1"] = numpy.nanmax(lims["mid_tot/det"]) 
        dataframe["mid_tot/pht_lims_0"] = numpy.nanmin(lims["mid_tot/pht"]) 
        dataframe["mid_tot/pht_lims_1"] = numpy.nanmax(lims["mid_tot/pht"]) 
        dataframe["avg_tau+_lims_0"] = numpy.nanmin(lims["avg_tau+"]) 
        dataframe["avg_tau+_lims_1"] = numpy.nanmax(lims["avg_tau+"]) 
        dataframe["avg_tau-_lims_0"] = numpy.nanmin(lims["avg_tau-"]) 
        dataframe["avg_tau-_lims_1"] = numpy.nanmax(lims["avg_tau-"]) 
        dataframe["avg_tot/det_lims_0"] = numpy.nanmin(lims["avg_tot/det"]) 
        dataframe["avg_tot/det_lims_1"] = numpy.nanmax(lims["avg_tot/det"]) 
        dataframe["avg_tot/pht_lims_0"] = numpy.nanmin(lims["avg_tot/pht"]) 
        dataframe["avg_tot/pht_lims_1"] = numpy.nanmax(lims["avg_tot/pht"]) 

        dataframe.to_pickle(os.path.join(target, "joint_dataframe"))
