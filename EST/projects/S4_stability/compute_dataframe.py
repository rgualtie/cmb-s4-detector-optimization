#!/usr/bin/python
import sys, os
import numpy
import pandas

from helper_funcs import *

nan = numpy.nan

spec = int(sys.argv[1]) # Choice in GHz det
tes = int(sys.argv[2])  # 0 for science, 1 for lab transition
Ic = float(sys.argv[3]) # Critical current [A]
if tes == 0: mode = "sci"
else: mode = "lab"

det, nm = load_det(spec, transition=mode)
det.TESs[0].Ic = Ic
det.TESs[1].Ic = Ic
det = det.copy_detector()

R_arr=numpy.arange(1e-3, 2.1e-2, 1e-3)
L_arr=numpy.arange(1e-7, 3.2e-6, 1e-7)
f_arr=[.3, .4, .5, .6, .7]

arr = []
for r in R_arr:
    for r_frac in f_arr:

        ndet = det.copy_detector()
        ndet.TESs[0].Rn = r
        ndet.TESs[1].Rn = 2.*r
        success = ndet.find_bias(R_frac=r_frac, TES_index=tes)
        if not success: 
            print spec, mode, 
            print ": failed find_bias for %.2e Ohms" % r

        for l in L_arr:

            ndet.L = l
            nm.det = ndet
        
            res = {}
            res["spec"] = spec
            res["mode"] = mode
            res["Ic"] = Ic
            res["r_frac"] = r_frac
            res["sci_Rn"] = r 
            res["lab_Rn"] = 2.*r 
            res["L"] = l
            res["stable"] = nan
            res["overdamped"] = nan
            res["Vb"] = nan
            res["R"] = nan
            res["I"] = nan
            res["T"] = nan
            res["betaI"] = nan
            res["alphaI"] = nan
            res["tau+"] = nan 
            res["tau-"] = nan 
            res["NEP_tot"] = nan 
            res["NEP_det"] = nan
            res["NEP_pht"] = nan

            if not success:
                arr.append(res)
                continue

            stable = ndet.stable()
            overdamped = ndet.overdamped()
            res["stable"] = stable
            res["overdamped"] = overdamped 

            res["Vb"] = ndet.Vb
            res["R"] = ndet.R
            res["I"] = ndet.I
            res["T"] = ndet.T

            res["betaI"] = ndet.get_beta()
            res["alphaI"] = ndet.get_alpha()

            taus = ndet.get_taus()
            res["tau+"] = taus[0]
            res["tau-"] = taus[1]

            NEPs = nm.total_multiplexed_noise()
            res["NEP_tot"] = NEPs[0]
            res["NEP_det"] = NEPs[1]
            NEP_photon = nm.dP_to_NEP(*nm.photon_noise())
            res["NEP_pht"] = NEP_photon(2.*numpy.pi) # evaluated at 1Hz
            arr.append(res)            
        
dataframe = pandas.DataFrame(arr)

dirname = dataframe_dir(spec, mode, Ic, mkdir=True)[-1]
savename = os.path.join(dirname, "dataframe")
dataframe.to_pickle(savename)
print spec, mode, Ic 
print ": saving ", savename


