#!/bin/python

import matplotlib
matplotlib.use('Agg')

import pandas

from helper_funcs import *
from dataset_tools import dataset 

spec = int(sys.argv[1])
mode = int(sys.argv[2])
Ic = float(sys.argv[3]) # Critical current [A]
if mode == 0: mode = "sci"
else: mode = "lab"

_, _, framename, savename = dataframe_dir(spec, mode, Ic)
dataframe = pandas.read_pickle(os.path.join(framename, "joint_dataframe"))
subdata = dataframe[dataframe["spec"] == spec]
data = dataset(subdata, mode=mode)

title_append1 = r" for %dGHz: $I_c=%dmA$" % (spec, 1e3*Ic)
title_append2 = "\n(%s config)" % (mode)
title_append2+= r" (lab Rn = 2 $\times$ sci Rn)"
title = lambda s: s + title_append1 + title_append2

plot_RL_args = [data.R_arr, data.L_arr]
plot_Rf_args = [data.R_arr, data.f_arr]
plot_kwargs = {"mode":mode}

viability = data.average_RL_grid("stable") / 2.
viability+= data.average_RL_grid("overdamped") / 2.
plot_kwargs["title"] = title("Viability")
plot_kwargs["lims"] = (0, 1)
plot_kwargs["cmap"] = plt.cm.viridis
plot_kwargs["savename"] = os.path.join(savename, "viability.png")
grid_plot_RL(viability, *plot_RL_args, **plot_kwargs) 

biases = data.extract_Rf_grid("Vb")
lims = data.extract_lims("Vb") 
plot_kwargs["title"] = title("Bias")
plot_kwargs["units"] = r"$[V]$"
plot_kwargs["lims"] = lims 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "Vb.png")
grid_plot_Rf(biases, *plot_Rf_args, **plot_kwargs) 
             
resistance = 1e3*data.extract_Rf_grid("R")
lims = data.extract_lims("R") 
plot_kwargs["title"] = title("Resistance")
plot_kwargs["units"] = r"$[m\Omega]$"
plot_kwargs["lims"] = (1e3*lims[0], 1e3*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "R.png")
grid_plot_Rf(resistance, *plot_Rf_args, **plot_kwargs) 

current = 1e6*data.extract_Rf_grid("I")
lims = data.extract_lims("I") 
plot_kwargs["title"] = title("Current")
plot_kwargs["units"] = r"$[\mu A]$"
plot_kwargs["lims"] = (1e6*lims[0], 1e6*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "I.png")
grid_plot_Rf(current, *plot_Rf_args, **plot_kwargs) 

temperature = 1e3*data.extract_Rf_grid("T")
lims = data.extract_lims("T") 
plot_kwargs["title"] = title("Temperature")
plot_kwargs["units"] = r"$[mK]$"
plot_kwargs["lims"] = (1e3*lims[0], 1e3*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "T.png")
grid_plot_Rf(temperature, *plot_Rf_args, **plot_kwargs) 

betaI = data.extract_Rf_grid("betaI")
lims = data.extract_lims("betaI") 
plot_kwargs["title"] = title(r"$\beta_I$")
plot_kwargs["units"] = ""
plot_kwargs["lims"] = lims 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "betaI.png")
grid_plot_Rf(betaI, *plot_Rf_args, **plot_kwargs) 

alphaI = data.extract_Rf_grid("alphaI")
lims = data.extract_lims("alphaI") 
plot_kwargs["title"] = title(r"$\alpha_I$")
plot_kwargs["units"] = ""
plot_kwargs["lims"] = lims 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "alphaI.png")
grid_plot_Rf(alphaI, *plot_Rf_args, **plot_kwargs) 

mid_tau_p = 1e3*numpy.real(data.extract_RL_grid("tau+"))
mid_tau_p = data.apply_mask(mid_tau_p) 
lims = data.extract_lims("mid_tau+") 
plot_kwargs["title"] = title(r"$\tau_+$ @ $R=R_n/2$")
plot_kwargs["units"] = "ms"
plot_kwargs["lims"] = (1e3*lims[0], 1e3*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "mid_tau_p.png")
grid_plot_RL(mid_tau_p, *plot_RL_args, **plot_kwargs) 

avg_tau_p = 1e3*numpy.real(data.average_RL_grid("tau+"))
avg_tau_p = data.apply_mask(avg_tau_p)
lims = data.extract_lims("avg_tau+") 
plot_kwargs["title"] = title(r"$\tau_+$ (average)")
plot_kwargs["units"] = "ms"
plot_kwargs["lims"] = (1e3*lims[0], 1e3*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "avg_tau_p.png")
grid_plot_RL(avg_tau_p, *plot_RL_args, **plot_kwargs) 

mid_tau_m = 1e3*numpy.real(data.extract_RL_grid("tau-"))
mid_tau_m = data.apply_mask(mid_tau_m)
lims = data.extract_lims("mid_tau-") 
plot_kwargs["title"] = title(r"$\tau_-$ @ $R=R_n/2$")
plot_kwargs["units"] = "ms"
plot_kwargs["lims"] = (1e3*lims[0], 1e3*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "mid_tau_m.png")
grid_plot_RL(mid_tau_m, *plot_RL_args, **plot_kwargs) 

mid_tau_m = 1e3*numpy.real(data.extract_RL_grid("tau-"))
mid_tau_m = data.apply_mask(mid_tau_m)
lims = data.extract_lims("mid_tau-") 
plot_kwargs["title"] = title(r"$\tau_-$ @ $R=R_n/2$")
plot_kwargs["units"] = "ms"
plot_kwargs["lims"] = (0, 1)
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "mid_tau_m_2.png")
grid_plot_RL(mid_tau_m, *plot_RL_args, **plot_kwargs) 

avg_tau_m = 1e3*numpy.real(data.average_RL_grid("tau-"))
avg_tau_m = data.apply_mask(avg_tau_m)
lims = data.extract_lims("avg_tau-") 
plot_kwargs["title"] = title(r"$\tau_-$ (average)")
plot_kwargs["units"] = "ms"
plot_kwargs["lims"] = (1e3*lims[0], 1e3*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "avg_tau_m.png")
grid_plot_RL(avg_tau_m, *plot_RL_args, **plot_kwargs) 

avg_tau_m = 1e3*numpy.real(data.average_RL_grid("tau-"))
avg_tau_m = data.apply_mask(avg_tau_m)
lims = data.extract_lims("avg_tau-") 
plot_kwargs["title"] = title(r"$\tau_-$ (average)")
plot_kwargs["units"] = "ms"
plot_kwargs["lims"] = (0, 1) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "avg_tau_m_2.png")
grid_plot_RL(avg_tau_m, *plot_RL_args, **plot_kwargs) 

mid_tot_det = 100*data.extract_RL_grid("tot/det")
mid_tot_det = data.apply_mask(mid_tot_det)
lims = data.extract_lims("mid_tot/det") 
plot_kwargs["title"] = title(r"$NEP_{det+readout}/NEP_{det}$ @ $R=R_n/2$")
plot_kwargs["units"] = r"%"
plot_kwargs["lims"] = (100*lims[0], 100*lims[1]) 
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "mid_readout_penalty.png")
grid_plot_RL(mid_tot_det, *plot_RL_args, **plot_kwargs) 

mid_tot_pht = 100*data.extract_RL_grid("tot/pht")
mid_tot_pht = data.apply_mask(mid_tot_pht)
lims = data.extract_lims("mid_tot/pht") 
plot_kwargs["title"] = title(r"$NEP_{det+readout}/NEP_{\gamma}$ @ $R=R_n/2$")
plot_kwargs["units"] = r"%"
plot_kwargs["lims"] = (100*lims[0], 100*lims[1])
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "mid_photon_sensitivity.png")
grid_plot_RL(mid_tot_pht, *plot_RL_args, **plot_kwargs) 

avg_tot_det = 100*data.average_RL_grid("tot/det")
avg_tot_det = data.apply_mask(avg_tot_det)
lims = data.extract_lims("avg_tot/det") 
plot_kwargs["title"] = title(r"$NEP_{det+readout}/NEP_{det}$ @ $R=R_n/2$")
plot_kwargs["units"] = r"%"
plot_kwargs["lims"] = (100*lims[0], 100*lims[1])
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "avg_readout_penalty.png")
grid_plot_RL(avg_tot_det, *plot_RL_args, **plot_kwargs) 

avg_tot_pht = 100*data.average_RL_grid("tot/pht")
avg_tot_pht = data.apply_mask(avg_tot_pht)
lims = data.extract_lims("avg_tot/pht") 
plot_kwargs["title"] = title(r"$NEP_{det+readout}/NEP_{\gamma}$ @ $R=R_n/2$")
plot_kwargs["units"] = r"%"
plot_kwargs["lims"] = (100*lims[0], 100*lims[1])
plot_kwargs["cmap"] = plt.cm.nipy_spectral
plot_kwargs["savename"] = os.path.join(savename, "avg_photon_sensitivity.png")
grid_plot_RL(avg_tot_pht, *plot_RL_args, **plot_kwargs) 


