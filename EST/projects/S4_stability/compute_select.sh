#!/bin/bash

cd /home/osherso2/documents/EST/projects/S4_stability

EST=/home/osherso2/documents/EST/
PYTHONPATH=$PYTHONPATH:$EST/physics_models
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability/det_lib
export PYTHONPATH

a="append"
e="/home/osherso2/documents/EST/projects/S4_stability/logs/compute_err"
o="/home/osherso2/documents/EST/projects/S4_stability/logs/compute_out"
s="270"
n="com"$s"sci" 
q="secondary"; w="04:00:00"
sbatch -p $q -t $w -J $n -e $e -o $o --open-mode=$a ./compute_dataframe.py $s 0;
q="astro-physics"; w="06:00:00"
sbatch -p $q -t $w -J $n -e $e -o $o --open-mode=$a ./compute_dataframe.py $s 0;

