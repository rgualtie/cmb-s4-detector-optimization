#!/bin/bash

cd /home/osherso2/documents/EST/projects/S4_stability

EST=/home/osherso2/documents/EST/
PYTHONPATH=$PYTHONPATH:$EST/physics_models
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability
PYTHONPATH=$PYTHONPATH:$EST/projects/S4_stability/det_lib
export PYTHONPATH

a="append"
e="/home/osherso2/documents/EST/projects/S4_stability/logs/plot_err"
o="/home/osherso2/documents/EST/projects/S4_stability/logs/plot_out"
rm $e
rm $o
#q="astro-physics"; w="06:00:00"
q="secondary"; w="03:20:00"
args=" -e "$e" -o "$o" --open-mode="$a
for Ic in .1 .05 .01
do
    for s in 30 40 85 95 145 155 220 270
    do
        n="plt"$s"sci"
        sbatch $args -p $q -t $w -J $n ./make_plots.py $s 0 $Ic
        n="plt"$s"lab"
        sbatch $args -p $q -t $w -J $n ./make_plots.py $s 1 $Ic
    done
done

