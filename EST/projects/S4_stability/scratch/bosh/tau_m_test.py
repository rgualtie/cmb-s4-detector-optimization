#!/usr/bin/python
import sys, os
import numpy
import pandas

from helper_funcs import *

nan = numpy.nan

spec = 30 
tes = 0 
if tes == 0: mode = "sci"
else: mode = "lab"

det, nm = load_det(spec, transition=mode, lib_dir="../det_lib")

R_arr = [3e-3]
L_arr = [1e-7, 2e-7, 3e-7, 4e-7, 5e-7]
f_arr=[.5]

for r in R_arr:
    for r_frac in f_arr:

        ndet = det.copy_detector()
        ndet.TESs[0].Rn = r
        ndet.TESs[1].Rn = 2.*r
        success = ndet.find_bias(R_frac=r_frac, TES_index=tes)
        ndet.print_state()
        for l in L_arr:

            ndet.L = l
            ndet = ndet.copy_detector()
            nm.det = ndet
       
            taus = ndet.get_taus()
            print l, taus[1]
