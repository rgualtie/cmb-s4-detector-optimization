import sys, numpy

import warnings
warnings.filterwarnings("ignore")

from helper_funcs import *

Ic = .01 # Amps

R_fracs = [.05, .1, .2, .3, .4, .5, .6, .7, .8, .9, .95]

sci_Rn = 1.7e-2
lab_Rn = 3e-2

min_args = {}
min_args["method"] = "SLSQP"
min_args["options"] = {"ftol":1e-12, "eps":1e-9, 
                       "maxiter":1000, "finite_diff_rel_step":1e-3}

def measure(det, target=1.):
    error = (det.R/target - 1.)
    P = det.P_bal()
    return "%03.03f%%, %.02eW\n" % (error, P)

score = 0
total = 0
for s in specs:
    for r in R_fracs:

                sci_target = det.Rl + r*sci_Rn
                lab_target = det.Rl + sci_Rn + r*lab_Rn

                det, _ = load_det(s, "sci", lib_dir="../det_lib")
                det.TESs[0].Ic = Ic
                det.TESs[1].Ic = Ic
                det.TESs[0].Rn = sci_Rn 
                det.TESs[1].Rn = lab_Rn 
                det = det.copy_detector()
                total += 1
                print s, r, 1 
                sucess = det.find_bias(R_frac=r, TES_index=0, verbose=False)
                if sucess: score += 1
                else: print"failed" 
                print "(%03d\%03d)" % (score, total)
                print measure(det, target=sci_target) 

                det, _ = load_det(s, "sci", lib_dir="../det_lib")
                det.TESs[0].Ic = Ic
                det.TESs[1].Ic = Ic
                det.TESs[0].Rn = sci_Rn 
                det.TESs[1].Rn = lab_Rn 
                det = det.copy_detector()
                total += 1
                print s, r, 2 
                sucess = det.find_bias(R_frac=r, TES_index=1, verbose=True)
                if sucess: score += 1
                else: print"failed" 
                print "(%03d\%03d)" % (score, total)
                print measure(det, target=lab_target) 

                det, _ = load_det(s, "lab", lib_dir="../det_lib")
                det.TESs[0].Ic = Ic
                det.TESs[1].Ic = Ic
                det.TESs[0].Rn = sci_Rn 
                det.TESs[1].Rn = lab_Rn 
                det = det.copy_detector()
                total += 1
                print s, r, 3 
                sucess = det.find_bias(R_frac=r, TES_index=1, verbose=False)
                if sucess: score += 1
                else: print "failed"
                print "(%03d\%03d)" % (score, total)
                print measure(det, target=lab_target) 

print score, total
