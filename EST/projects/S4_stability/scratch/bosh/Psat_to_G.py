#!/bin/python

import numpy

Tb = .1 # K
Tc = .16 # K
Tx = .16 # K (reference temperature)
betaG = 1.7 # a guess

def G(Psat):
    eta = betaG + 1.
    G = Psat * eta * Tx**betaG
    G/= Tc**eta - Tb**eta
    return G

Psat = {}
Psat[30] = 1.3e-12
Psat[40] = 2.8e-12
Psat[85] = 5.5e-12
Psat[95] = 5.5e-12
Psat[145] = 8.3e-12
Psat[155] = 8.3e-12
Psat[220] = 15.8e-12
Psat[270] = 21.8e-12

for spec in numpy.sort(Psat.keys()):
    print spec, G(Psat[spec])

