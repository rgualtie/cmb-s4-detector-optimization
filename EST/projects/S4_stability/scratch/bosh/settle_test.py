import sys, numpy

import warnings
warnings.filterwarnings("ignore")

from helper_funcs import *
det, _ = load_det(int(sys.argv[1]), sys.argv[2])

print
print " ---- "
print 

R_fracs = [.01, .05, .1, .2, .3, .4, .5, .6, .7, .8, .9, .95, .99]

R_target = []
V_target = []

for i in [0, 1]:
    print "TES: ", i
    for r in R_fracs:

        if i == 0: R = det.Rl + det.TESs[0].Rn * r
        elif i == 1: R = det.Rl + det.TESs[0].Rn + r*det.TESs[1].Rn

        print "Finding Vb for R: %.03e Ohms" % R
        try: det.find_bias(R_frac=r, TES_index=i)
        except Exception as e:
            print "Error in stationary point finder\n"
            continue
        R_target.append(det.R)
        V_target.append(det.Vb)
        print
    print " ---- "
print

for r, v in zip(R_target, V_target):

    print "Settling V: %.03eV (R: %.3e Ohms)" % (v, r)
    det.settle(v)
    err = 100.*abs(det.R/r - 1.)
    if err > 1: print "Failed to settle (err = %03.03f%%)." % err
    print "R found: %.03e Ohms, error: %03.03f%%" % (det.R, err)
    print "P_bal: %.02e W" % det.P_bal()
    print 
