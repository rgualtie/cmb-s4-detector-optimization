import sys, numpy

import warnings
warnings.filterwarnings("ignore")

from helper_funcs import *


h  = 6.62607e-34 # J s
c  = 2.99792e8   # m/s
kB = 1.38065e-23 # J/K
def Popt(T, center_freq, bandwidth, optical_efficiency=.3):

    band = [center_freq - bandwidth/2., center_freq + bandwidth/2.]
    nu_arr = numpy.logspace(numpy.log10(band[0]), numpy.log10(band[1]), 100)
    B_nu = lambda v, T: ((h/c**2.)*(v**3.)/(numpy.exp(h*v/(kB*T))-1.))*(c/v)**2.
    P_bw = lambda T: numpy.trapz(B_nu(nu_arr, T), x=nu_arr)
    return P_bw(T)*optical_efficiency

for s in specs:

    det, _ = load_det(s, lib_dir="../det_lib")
    print s, Popt(300., det.center_freq, det.bandwidth)




