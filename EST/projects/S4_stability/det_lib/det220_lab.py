# Detector saved on 2020-11-23 12:05:17.316540

from TES_model import *
from DET_model import *

# TESs
TESs = []; Tcs = []
tes00_params = {}
tes00_params["name"] = "sci" # name
tes00_params["Ic"] = .1 # crit current [A]
tes00_params["Tc"] = 1.6e-01 # crit temp [K]
tes00_params["Rn"] = 1.0e-02 # normal res [Ohms]
tes00_params["alpha0"] = 2.0e+02 # (dln(R)/dln(T) | T=Tc,I=0)
# State of TES (at moment of save)
tes00_params["I"] = 0.0 # [A]
tes00_params["T"] = 0.0 # [K]
tes00 = TES(**tes00_params)
TESs.append(tes00)
Tcs.append(1.6e-01)

tes01_params = {}
tes01_params["name"] = "lab" # name
tes01_params["Ic"] = .1 # crit current [A]
tes01_params["Tc"] = 4.5e-01 # crit temp [K]
tes01_params["Rn"] = 2.0e-02 # normal res [Ohms]
tes01_params["alpha0"] = 1.0e+02 # (dln(R)/dln(T) | T=Tc,I=0)
# State of TES (at moment of save)
tes01_params["I"] = 0.0 # [A]
tes01_params["T"] = 0.0 # [K]
tes01 = TES(**tes01_params)
TESs.append(tes01)
Tcs.append(4.5e-01)
TESs = numpy.array(TESs)[numpy.argsort(Tcs)]

# Detector
det_params = {}
det_params["TESs"] = TESs
det_params["Tx"] = 1.6e-01 # Reference temp for C, G [K]
det_params["Cx"] = 3.0e-12 # Heat capacity at Tx [J/K]
det_params["Gx"] = 3.7088e-10 # Therm conductance at Tx [W/K]
det_params["betaC"] = 2.3 # Exponent on heat capacity
det_params["betaG"] = 1.7 # Exponent on therm conductivity
det_params["num_rows"] = 64 # Number of equiv dets per bias line
det_params["L"] = 1.0e-07 # Inductance facing det [H]
det_params["Rsh"] = 2.5e-04 # Shunt resistance [Ohms]
det_params["Rb"] = 200. # Bias resistor [Ohms]
det_params["Rl"] = 0.0 # Parasitic res on island [Ohms]
det_params["Rs"] = 0.0 # Parasitic res off island [Ohms]
det_params["Popt"] = 5.909e-11 # Optical power on island [W]
det_params["center_freq"] = 2.200e+11 # Center freq of observing band [Hz]
det_params["bandwidth"] = 4.84e+10 # Width of observing band [Hz]
# State of det (at moment of save)
det_params["Vb"] = 0.0 # Bias voltage [V]
det_params["Tb"] = 0.1 # Substrate temp [K]
det_params["R"] = 0.0 # Total island res [Ohms]
det_params["I"] = 0.0 # [A]
det_params["T"] = 0.0 # [K]
det220_lab = detector(**det_params)
det220_lab.halfway_finder(TES_index=1)
det220_lab.print_state()

from NOISE_model import *

#Noise model
noise_params = {}
noise_params["det"] = det220_lab
noise_params["readout_plateau"] = 2.5e-12 # Noise plateau NEI [A/sqrt(Hz)]
noise_params["pink_knee"] = 2. # [Hz]
noise_params["roll_off"] = 2.5e-6 # [Hz]
noise_params["m"] = 2. # Excess noise parameter
noise_params["dwell_by_tau"] = 6. # Num of SQUID t.c. to dwell on det.
noise220_lab = noise(**noise_params)

if __name__ == "__main__": 
    
    det220_lab.save_pkl("det220_lab.pkl")
