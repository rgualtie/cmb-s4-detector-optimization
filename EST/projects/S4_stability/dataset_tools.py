#!/bin/python
import sys, os
import numpy

nan = numpy.nan

specs = [30, 40, 85, 95, 145, 155, 220, 270]

class dataset:

    def __init__(self, dataframe, mode="sci"):

        self.dataframe = dataframe
        self.mode = mode

        self.R_arr = numpy.sort(numpy.unique(dataframe["%s_Rn" % mode]))
        self.L_arr = numpy.sort(numpy.unique(dataframe["L"]))
        self.f_arr = numpy.sort(numpy.unique(dataframe["r_frac"]))

        stable = self.average_RL_grid("stable")
        overdamped = self.average_RL_grid("overdamped")
        self.mask = numpy.logical_not(stable + overdamped == 2.)

    def extract_RL_grid(self, keyword, r_frac=.5):

        dataframe = self.dataframe
        R_arr = self.R_arr
        L_arr = self.L_arr

        arr = numpy.zeros((len(R_arr), len(L_arr)))

        ind1 = abs(dataframe["r_frac"] - r_frac)/r_frac < 1e-6
        assert(len(ind1[ind1]) > 0)
        for i, r in enumerate(R_arr): 
            ind2 = abs(dataframe["%s_Rn" % self.mode] - r)/r < 1e-6
            assert(len(ind2[ind2]) > 0)
            for j, l in enumerate(L_arr):
                ind3 = abs(dataframe["L"] - l)/l < 1e-6
                assert(len(ind3[ind3]) > 0)
                inds = numpy.logical_and(numpy.logical_and(ind1, ind2), ind3)
                assert(len(inds[inds]) > 0)

                subdata = dataframe[inds]
                arr[i, j] = float(subdata[keyword])

        return arr 

    def extract_Rf_grid(self, keyword, L=1e-7):

        dataframe = self.dataframe
        R_arr = self.R_arr
        f_arr = self.f_arr

        arr = numpy.zeros((len(R_arr), len(f_arr)))

        ind1 = abs(dataframe["L"] - L)/L < 1e-6 
        assert(len(ind1[ind1]) > 0)
        for i, r in enumerate(R_arr): 
            ind2 = abs(dataframe["%s_Rn" % self.mode] - r)/r < 1e-6 
            assert(len(ind2[ind2]) > 0)
            for j, f in enumerate(f_arr):
                ind3 = abs(dataframe["r_frac"] - f)/f < 1e-6
                assert(len(ind3[ind3]) > 0)
                inds = numpy.logical_and(numpy.logical_and(ind1, ind2), ind3)
                assert(len(inds[inds]) > 0)
    
                subdata = dataframe[inds]
                arr[i, j] = float(subdata[keyword])
    
        return arr 

    def average_RL_grid(self, keyword):

        dataframe = self.dataframe
        R_arr = self.R_arr
        L_arr = self.L_arr
        f_arr = self.f_arr
        N = float(len(f_arr))

        arr = None

        for r_frac in f_arr:
            subdata = self.extract_RL_grid(keyword, r_frac)
            subdata = subdata / N 
            if arr is None: arr = subdata.copy()
            else: arr += subdata

        return arr            

    def apply_mask(self, arr):

        arr_copy = arr.copy()
        arr_copy[self.mask] = nan
        return arr_copy

    def get_RL_lims(self, keyword_arr=["betaI"], r_frac=None, mask=True):

        lims = {}
        for k in keyword_arr: 

            lims[k] = (nan, nan)
            if r_frac is None: arr = self.average_RL_grid(k)
            else: arr = self.extract_RL_grid(k, r_frac=r_frac)
            if mask: arr = self.apply_mask(arr)

            try:
                a = numpy.nanmin(arr)
                a = float("%.2e" % a)
                lims[k] = (a, nan)
            except: lims[k] = (nan, nan)                
            try:
                b = numpy.nanmax(arr)
                b = float("%.2e" % b)
                lims[k] = (lims[k][0], b)
            except: lims[k] = (lims[k][0], nan)

        return lims

    def get_Rf_lims(self, keyword_arr=["betaI"], L=1e-7):

        lims = {}
        for k in keyword_arr: 

            arr = self.extract_Rf_grid(k, L=L)

            try:
                a = numpy.nanmin(arr)
                a = float("%.2e" % a)
                lims[k] = (a, nan)
            except: lims[k] = (nan, nan)                
            try:
                b = numpy.nanmax(arr)
                b = float("%.2e" % b)
                lims[k] = (lims[k][0], b)
            except: lims[k] = (lims[k][0], nan)

        return lims

    def extract_lims(self, keyword):

        k = keyword+"_lims"
        a = numpy.unique(self.dataframe[k+"_0"])
        assert(len(a) == 1); a = a[0]
        b = numpy.unique(self.dataframe[k+"_1"])
        assert(len(b) == 1); b = b[0]
        return (a, b)

