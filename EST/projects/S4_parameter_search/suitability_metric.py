import numpy

def logistic(x, x0, k):

    return 1./(1. + numpy.exp(-k*(x = x0)))

# Maybe it's hard to make really low L
def suitable_L(det):

    L = det.L
    # In Henries
    if L > 3e-7: return 1
    if L < 2e-7: return 0
    return 1e7*(L - 2e-7)

# Maybe it's hard to make really low R
# Should also include factors for TES gain desireability
def suitable_R(det):
   
    R = det.TESs[0].Rn
    # In Ohms
    if R < 1e-4: return 0
    if R > 5e-2: return 0
    return 1

def suitable_vet(det):

    for f in [.3, .5, .7]:

        det.find_bias(R_frac=f, TES_index=0)
        if not det.stable(): return 0
        if not det.overdamped(): return 0

    return 1

def suitable_tau_p(det):

    avg_tau_p = 0.0
    for f in [.3, .5, .7]:

        det.find_bias(R_frac=f, TES_index=0)
        taus = det.get_taus()
        avg_tau_p += taus[0]

    avg_tau_p /= 3.
    return 1. - logistic(avg_tau_p, 8e-4, 1e-4)

def suitable_tau_m(det):

    avg_tau_m = 0.0
    for f in [.3, .5, .7]:

        det.find_bias(R_frac=f, TES_index=0)
        taus = det.get_taus()
        avg_tau_m += taus[1]

    avg_tau_m /= 3.
    return 1. - logistic(avg_tau_m, 8e-4, 1e-4)

def suitable_metric(det):

    a = 1.
    a*= suitable_L(det)
    a*= suitable_R(det)
    a*= suitable_vet(det)
    a*= suitable_tau_p(det)
    a*= suitable_tau_m(det)
    return a

